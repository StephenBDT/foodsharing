<?php
global $g_lang;

$g_lang['title'] = 'Titel';
$g_lang['body'] = 'Nachricht';
$g_lang['forum'] = 'Forum';
$g_lang['bot_forum'] = 'BotschafterInnenforum';
$g_lang['compose_new_theme'] = 'Neues Thema verfassen';
$g_lang['empty_forum'] = 'Noch keine Themen gepostet';
$g_lang['delete_post'] = 'Beitrag löschen';
$g_lang['delete_sure'] = 'Soll der Beitrag wirklich gelöscht werden?';
$g_lang['delete_sure_title'] = 'Wirklich löschen?';
$g_lang['sure'] = 'Ja, ich bin mir sicher.';
$g_lang['signout'] = 'Austragen';
$g_lang['bezirk_signout'] = 'Bezirk/Team "{var}" verlassen';
$g_lang['signout_sure'] = 'Möchtest Du wirklich den Bezirk bzw. das Team "{var}" verlassen?';
$g_lang['signout_sure_title'] = 'Bist Du sicher?';
$g_lang['new_fairteiler'] = 'Neuer Fair-Teiler';
$g_lang['list_fairteiler'] = 'Alle Fair-Teiler in {var}';
$g_lang['no_fairteiler_available'] = 'Noch keine Fair-Teiler eingetragen';

$g_lang['older_themes'] = 'ältere Themen';
$g_lang['no_events_posted'] = 'Noch keine Events vorhanden';
$g_lang['new_event'] = 'Neue/n Termin/Veranstaltung eintragen';
$g_lang['description'] = 'Beschreibung';
$g_lang['desc_desc'] = 'Was ist das für ein Event?';
$g_lang['location_name'] = 'Veranstaltungsort';
$g_lang['online_type'] = 'Findet das Event offline oder online auf unserem Mumble-Server statt?';
$g_lang['offline'] = 'Ganz normal im echten Leben';
$g_lang['online'] = 'Multilokal auf mumble.lebensmittelretten.de';
$g_lang['mumble_room'] = 'In welchem mumble Konferenzraum treffen wir uns?';
$g_lang['dateend'] = 'Enddatum';
$g_lang['post_could_not_saved'] = 'Post konnte nicht gespeichert werden.';
