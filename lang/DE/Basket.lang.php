<?php
global $g_lang;
$g_lang['picture-desc'] = 'Füge dem Essenskorb ein Foto hinzu!';
$g_lang['picture-choose'] = 'Datei wählen';
$g_lang['description'] = 'Beschreibung';
$g_lang['weight'] = 'Geschätztes Gewicht';
$g_lang['food_type'] = 'Welche Arten von Lebensmitteln sind dabei?';
$g_lang['food_art'] = 'Was trifft auf die Lebensmittel zu?';
$g_lang['contactmessage'] = 'Per Nachricht';
$g_lang['fetchstate'] = 'Hat alles gut geklappt?';
$g_lang['contact_type'] = 'Wie möchtest Du kontaktiert werden?';
$g_lang['tel'] = 'Festnetztelefon';

$g_lang['no_requests'] = 'bisher keine Anfragen';
$g_lang['one_request'] = 'eine Anfrage';
$g_lang['req_count'] = '{count} Anfragen';
