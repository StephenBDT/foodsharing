<?php
global $g_lang;

$g_lang['apply_type'] = 'Wer kann sich für diese Gruppe eintragen?';
$g_lang['banana_count'] = 'Wie viele Vertrauensbananen braucht ein Mitglied?';
$g_lang['fetch_count'] = 'Wie viele Abholungen sollte ein Bewerber gemacht haben?';
$g_lang['week_num'] = 'Seit wie vielen Wochen sollte ein Bewerber schon dabei sein?';
$g_lang['report_num'] = 'Dürfen sich auch Foodsaver mit Meldungen von Regelverletzungen bewerben?';
$g_lang['member'] = 'Mitglieder';
$g_lang['leader'] = 'Gruppen-Admins';
$g_lang['contact-disclaimer'] = 'Durch Senden einer Nachricht an diese Arbeitsgruppe erhalten alle Administratoren der Arbeitsgruppe deine Emailadresse, welche du auf deinem foodsharing.de Profil angegeben hast.';
