<?php
global $g_lang;
$g_lang['language_id'] = 'Sprache';
$g_lang['name'] = 'Name';
$g_lang['body'] = 'Text';
$g_lang['bread_message_tpl'] = 'Alle Vorlagen';
$g_lang['bread_edit_message_tpl'] = 'E-Mail-Vorlage bearbeiten';
$g_lang['bread_new_message_tpl'] = 'neue Vorlage erstellen';
$g_lang['message_tpl_empty'] = 'Bisher wurden keine E-Mail-Vorlagen eingetragen.';
$g_lang['neu_message_tpl'] = 'Neue Vorlage eintragen';
$g_lang['delete_sure'] = 'Soll {var} wirklich unwiderruflich gel&ouml;scht werden?';
$g_lang['message_tpl_add_success'] = 'Vorlage wurde hinzugef&uuml;gt';
$g_lang['message_tpl'] = 'E-Mail-Vorlagen';
$g_lang['message_tpl_bread'] = 'E-Mail-Vorlagen';
$g_lang['message_tpl_edit_success'] = 'Vorlage wurde bearbeitet';
