import '@/core'
import '@/globals'
import 'jquery-tagedit'
import 'jquery-tagedit-auto-grow-input'
import 'jquery-dynatree'
import 'leaflet'
import 'leaflet.awesome-markers'
import 'leaflet.markercluster'

import { img } from '@/script'
import { expose } from '@/utils'

import './RegionAdmin.css'

expose({
  img
})
