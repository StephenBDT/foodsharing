<?php

namespace Foodsharing\Modules\Basket;

use Foodsharing\Lib\View\vMap;
use Foodsharing\Lib\View\vPage;
use Foodsharing\Modules\Core\View;

class BasketView extends View
{
	public function find($baskets, $location)
	{
		$page = new vPage('Essenskörbe', $this->findMap($location));

		if ($baskets) {
			$page->addSectionRight($this->closeBaskets($baskets), 'In Deiner Nähe');
		}

		$page->render();
	}

	private function findMap($location)
	{
		$map = new vMap($location);

		if (is_array($location)) {
			$map->setCenter($location['lat'], $location['lon']);
		}

		$map->setSearchPanel('mapsearch');
		$map->setMarkerCluster();
		$map->setDefaultMarkerOptions('basket', 'green');

		return '<div class="ui-widget"><input id="mapsearch" type="text" name="mapsearch" value="" placeholder="Adresssuche..." class="input text value ui-corner-top"/><div class="findmap">' . $map->render(
			) . '</div></div>';
	}

	public function closeBaskets($baskets)
	{
		$out = '
		<ul class="linklist" id="cbasketlist">';
		foreach ($baskets as $b) {
			$img = '/img/basket.png';
			if (!empty($b['picture'])) {
				$img = '/images/basket/thumb-' . $b['picture'];
			}

			$distance = $this->distance($b['distance']);

			$out .= '
				<li>
					<a class="ui-corner-all" onclick="ajreq(\'bubble\',{app:\'basket\',id:' . (int)$b['id'] . ',modal:1});return false;" href="#">
						<span style="float:left;margin-right:7px;"><img width="35px" src="' . $img . '" class="ui-corner-all"></span>
						<span style="height:35px;overflow:hidden;font-size:11px;line-height:16px;"><strong style="float:right;margin:0 0 0 3px;">(' . $distance . ')</strong>' . $this->func->tt(
					$b['description'],
					50
				) . '</span>
						
						<span style="clear:both;"></span>
					</a>
				</li>';
		}
		$out .= '
		</ul>
		<div style="text-align:center;">
			<a class="button" href="/karte?load=baskets">Alle Körbe auf der Karte</a>
		</div>';

		return $out;
	}

	public function basket($basket, $wallposts, $requests)
	{
		$page = new vPage(
			'Essenskorb #' . $basket['id'], '
		
		<div class="pure-g">
		    <div class="pure-u-1 pure-u-md-1-3">
				' . $this->pageImg($basket['picture']) . '	
			</div>
		    <div class="pure-u-1 pure-u-md-2-3">
				<p>' . nl2br($basket['description']) . '</p>
			</div>
		</div>
		'
		);

		$page->setSubTitle(
			'<p>Veröffentlicht am <strong>' . $this->func->niceDate(
				$basket['time_ts']
			) . '</strong></p><p>Gültig bis <strong>' . $this->func->niceDate($basket['until_ts']) . '</strong></p>'
		);

		if ($wallposts) {
			$page->addSection($wallposts, 'Pinnwand');
		}
		if ($this->session->may()) {
			$page->addSectionRight($this->userBox($basket), 'AnbieterIn');

			if ($basket['lat'] != 0 || $basket['lon'] != 0) {
				$map = new vMap([$basket['lat'], $basket['lon']]);
				$map->addMarker($basket['lat'], $basket['lon']);

				$map->setDefaultMarkerOptions('basket', 'green');

				$map->setCenter($basket['lat'], $basket['lon']);

				$page->addSectionRight($map->render(), 'Wo?');
			}

			if ($basket['fs_id'] == $this->session->id() && $requests) {
				$page->addSectionRight($this->requests($requests), count($requests) . ' Anfragen');
			}
		} else {
			$page->addSectionRight(
				$this->v_utils->v_info('Für detaillierte Infos musst Du eingeloggt sein. Bist du noch nicht als Foodsharer registriert? Dann klick oben auf <b>Mach mit!</b> um loszulegen!', 'Hinweis!'),
				false,
				array('wrapper' => false)
			);
		}

		$page->render();
	}

	public function basketTaken($basket)
	{
		$page = new vPage(
			'Essenskorb #' . $basket['id'], '
		
		<div class="pure-g">
		    <div class="pure-u-1 pure-u-md-2-3">
				<p>Dieser Essenskorb wurde bereits abgeholt</p>
			</div>
		</div>
		'
		);
		$page->render();
	}

	public function requests($requests)
	{
		$out = '
		<ul class="linklist conversation-list">';

		foreach ($requests as $r) {
			$out .= '
			<li><a onclick="chat(' . (int)$r['fs_id'] . ');return false;" href="#"><span class="pics"><img width="50" alt="avatar" src="' . $this->func->img(
					$r['fs_photo']
				) . '"></span><span class="names">' . $r['fs_name'] . '</span><span class="msg"></span><span class="time">' . $this->func->niceDate(
					$r['time_ts']
				) . '</span><span class="clear"></span></a></li>';
		}

		$out .= '
		</ul>';

		return $out;
	}

	private function userBox($basket)
	{
		$request = '';

		if ($basket['fs_id'] != $this->session->id()) {
			$request = '<div><a class="button button-big" href="#" onclick="ajreq(\'request\',{app:\'basket\',id:' . (int)$basket['id'] . '});">Essenskorb anfragen</a>	</div>';
		} else {
			$request = '<div><a class="button button-big" href="#" onclick="ajreq(\'removeBasket\',{app:\'basket\',id:' . (int)$basket['id'] . '});">Essenskorb löschen</a>	</div>';
		}

		return $this->fsAvatarList(
				array(
					array(
						'id' => $basket['fs_id'],
						'name' => $basket['fs_name'],
						'photo' => $basket['fs_photo'],
						'sleep_status' => $basket['sleep_status'],
					),
				),
				array('height' => 60, 'scroller' => false)
			) .
			$request;
	}

	private function pageImg($img): string
	{
		if ($img != '') {
			return '<img class="basket-img" src="/images/basket/medium-' . $img . '" />';
		}

		return '<img class="basket-img" src="/img/foodloob.gif" />';
	}

	public function basketForm($foodsaver): string
	{
		$out = '';

		$out .= $this->v_utils->v_form_textarea('description', array('maxlength' => 1705));

		$values = [
			['id' => 0.25, 'name' => '250 g'],
			['id' => 0.5, 'name' => '500 g'],
			['id' => 0.75, 'name' => '750 g'],
		];

		for ($i = 1; $i <= 10; ++$i) {
			$values[] = [
				'id' => $i,
				'name' => number_format($i, 2, ',', '.') . '<span style="white-space:nowrap">&thinsp;</span>kg',
			];
		}

		for ($i = 2; $i <= 10; ++$i) {
			$val = ($i * 10);
			$values[] = [
				'id' => $val,
				'name' => number_format($val, 2, ',', '.') . '<span style="white-space:nowrap">&thinsp;</span>kg',
			];
		}

		$out .= $this->v_utils->v_form_select(
			'weight',
			[
				'values' => $values,
				'selected' => 3,
			]
		);

		$out .= $this->v_utils->v_form_checkbox(
			'contact_type',
			[
				'values' => [
					['id' => 1, 'name' => 'Per Nachricht'],
					['id' => 2, 'name' => 'Per Telefonanruf'],
				],
				'checked' => [1],
			]
		);

		$out .= $this->v_utils->v_form_text('tel', ['value' => $foodsaver['telefon']]);
		$out .= $this->v_utils->v_form_text('handy', ['value' => $foodsaver['handy']]);

		$out .= $this->v_utils->v_form_checkbox(
			'food_type',
			[
				'values' => [
					['id' => 1, 'name' => 'Backwaren'],
					['id' => 2, 'name' => 'Obst & Gemüse'],
					['id' => 3, 'name' => 'Molkereiprodukte'],
					['id' => 4, 'name' => 'Trockenware'],
					['id' => 5, 'name' => 'Tiefkühlware'],
					['id' => 6, 'name' => 'Zubereitete Speisen'],
					['id' => 7, 'name' => 'Tierfutter'],
				],
			]
		);

		$out .= $this->v_utils->v_form_checkbox(
			'food_art',
			[
				'values' => [
					['id' => 1, 'name' => 'sind Bio'],
					['id' => 2, 'name' => 'sind vegetarisch'],
					['id' => 3, 'name' => 'sind vegan'],
					['id' => 4, 'name' => 'sind glutenfrei'],
				],
			]
		);

		return $out;
	}

	public function contactMsg(): string
	{
		return $this->v_utils->v_form_textarea('contactmessage');
	}

	public function contactTitle($basket): string
	{
		return '<img src="' . $this->func->img($basket['fs_photo']) . '" style="float:left;margin-right:15px;" />
		<p>' . $basket['fs_name'] . ' kontaktieren</p>
		<div style="clear:both;"></div>';
	}

	public function contactNumber($basket): string
	{
		$out = '';
		$content = '';
		if (!empty($basket['tel'])) {
			$content .= ('<tr><td>Festnetz: &nbsp;</td><td>' . $basket['tel'] . '</td></tr>');
		}
		if (!empty($basket['handy'])) {
			$content .= ('<tr><td>Handy: &nbsp;</td><td>' . $basket['handy'] . '</td></tr>');
		}
		if (!empty($content)) {
			$out .= $this->v_utils->v_input_wrapper('Telefonisch kontaktieren', '<table>' . $content . '</table>');
		}

		return $out;
	}

	public function fsBubble($basket)
	{
		$img = '';
		if (!empty($basket['picture'])) {
			$img = '<div style="width:100%;max-height:200px;overflow:hidden;"><img src="http://media.myfoodsharing.org/de/items/200/' . $basket['picture'] . '" /></div>';
		}

		return '
		' . $img . '
		' . $this->v_utils->v_input_wrapper('Beschreibung', nl2br($this->func->autolink($basket['description']))) . '
		' .
			'<div style="text-align:center;"><a class="fsbutton" href="' . BASE_URL . '/essenskoerbe/' . $basket['fsf_id'] . '" target="_blank">Essenskorb anfragen auf foodsharing.de</a></div>';
	}

	public function bubbleNoUser($basket): string
	{
		$img = '';
		if (!empty($basket['picture'])) {
			$img = '<div style="width:100%;overflow:hidden;"><img src="/images/basket/medium-' . $basket['picture'] . '" width="100%" /></div>';
		}

		return '
		' . $img . '
		' . $this->v_utils->v_input_wrapper('Beschreibung', nl2br($this->func->autolink($basket['description']))) . '
		';
	}

	public function bubble($basket): string
	{
		$img = '';
		if (!empty($basket['picture'])) {
			$img = '<div style="width:100%;overflow:hidden;"><img src="/images/basket/medium-' . $basket['picture'] . '" width="100%" /></div>';
		}

		return '
		' . $img . '
		' . $this->v_utils->v_input_wrapper('Einstelldatum', $this->func->niceDate($basket['time_ts'])) . '
		' . $this->v_utils->v_input_wrapper('Beschreibung', nl2br($this->func->autolink($basket['description']))) . '
		';
	}
}
