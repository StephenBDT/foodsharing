
<div align="center">
	<br>
	<br>
	<a href="https://foodsharing.de">
		<!-- image is docs/images/FS_Logo_gb_RGB.png but hosted publicly -->
		<img src="https://user-images.githubusercontent.com/31616/42413241-8802b03c-821c-11e8-91c5-f94930313290.png" alt="foodsharing">
	</a>
	<br>
	<br>
</div>

<!-- image is docs/images/fsscreenshot.png but hosted pubicly -->
![](https://user-images.githubusercontent.com/31616/42418486-f8f571e6-82a1-11e8-8771-41e403944101.png)

## 🌍 Overview

This is the code that powers
[foodsharing.de](https://foodsharing.de), 
[foodsharing.at](https://foodsharing.at), and
[foodsharingschweiz.ch](https://foodsharingschweiz.ch).

## 💻 Development

Visit [devdocs.foodsharing.de](https://devdocs.foodsharing.de) to get started with development.

## 💒 Community

Our developers hang out in the
[yunity slack](https://slackin.yunity.org) group in the
**#foodsharing-dev** and **#foodsharing-beta** channels. Come and say hi!

## 👪 Contributors

<!-- FOODSHARING-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<table border="0">
  <tbody>
    <tr border="0">
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/NerdyProjects">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/642557/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span>&nbsp;<span title="Infrastructure (Hosting, Build-Tools, etc)"><img width="14px" class="emoji" draggable="false" alt="🔩" src="https://twemoji.maxcdn.com/2/svg/1f529.svg"/></span>&nbsp;<span title="Reviewed Pull Requests"><img width="14px" class="emoji" draggable="false" alt="👀" src="https://twemoji.maxcdn.com/2/svg/1f440.svg"/></span><br>
        <a href="https://gitlab.com/NerdyProjects">
          <sub>Matthias Larisch</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/peter.toennies">
            <img src="https://foodsharing.de/images/q_f57a9d0c7fa4b1b889cfe3245310ad06.jpg" width="100px">
          </a><br>
        </div>
        <span title="Bug reports"><img width="14px" class="emoji" draggable="false" alt="🐜" src="https://twemoji.maxcdn.com/2/svg/1f41c.svg"/></span>&nbsp;<span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span>&nbsp;<span title="Ideas, Planning, & Feedback"><img width="14px" class="emoji" draggable="false" alt="💡" src="https://twemoji.maxcdn.com/2/svg/1f4a1.svg"/></span>&nbsp;<span title="Answering Questions"><img width="14px" class="emoji" draggable="false" alt="💬" src="https://twemoji.maxcdn.com/2/svg/1f4ac.svg"/></span>&nbsp;<span title="Reviewed Pull Requests"><img width="14px" class="emoji" draggable="false" alt="👀" src="https://twemoji.maxcdn.com/2/svg/1f440.svg"/></span><br>
        <a href="https://gitlab.com/peter.toennies">
          <sub>Peter Tönnies</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/k.miklobusec">
            <img src="https://foodsharing.de/images/q_e09cefb9259140ef547313b6cb5691ea.jpg" width="100px">
          </a><br>
        </div>
        <span title="Board member"><img width="14px" class="emoji" draggable="false" alt="🏢" src="https://twemoji.maxcdn.com/2/svg/1f3e2.svg"/></span>&nbsp;<span title="Bug reports"><img width="14px" class="emoji" draggable="false" alt="🐜" src="https://twemoji.maxcdn.com/2/svg/1f41c.svg"/></span>&nbsp;<span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span>&nbsp;<span title="Ideas, Planning, & Feedback"><img width="14px" class="emoji" draggable="false" alt="💡" src="https://twemoji.maxcdn.com/2/svg/1f4a1.svg"/></span>&nbsp;<span title="Answering Questions"><img width="14px" class="emoji" draggable="false" alt="💬" src="https://twemoji.maxcdn.com/2/svg/1f4ac.svg"/></span><br>
        <a href="https://gitlab.com/k.miklobusec">
          <sub>Once&#8203;Upon&#8203;A&#8203;Foodsharing&#8203;Time</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/nicksellen">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/640443/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span>&nbsp;<span title="Documentation"><img width="14px" class="emoji" draggable="false" alt="📝" src="https://twemoji.maxcdn.com/2/svg/1f4dd.svg"/></span>&nbsp;<span title="Infrastructure (Hosting, Build-Tools, etc)"><img width="14px" class="emoji" draggable="false" alt="🔩" src="https://twemoji.maxcdn.com/2/svg/1f529.svg"/></span>&nbsp;<span title="Reviewed Pull Requests"><img width="14px" class="emoji" draggable="false" alt="👀" src="https://twemoji.maxcdn.com/2/svg/1f440.svg"/></span><br>
        <a href="https://gitlab.com/nicksellen">
          <sub>Nick Sellen</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/tiltec">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/640465/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span>&nbsp;<span title="Reviewed Pull Requests"><img width="14px" class="emoji" draggable="false" alt="👀" src="https://twemoji.maxcdn.com/2/svg/1f440.svg"/></span><br>
        <a href="https://gitlab.com/tiltec">
          <sub>Tilmann Becker</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/michi-zuri">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/1682847/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Bug reports"><img width="14px" class="emoji" draggable="false" alt="🐜" src="https://twemoji.maxcdn.com/2/svg/1f41c.svg"/></span>&nbsp;<span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span>&nbsp;<span title="Design"><img width="14px" class="emoji" draggable="false" alt="🎨" src="https://twemoji.maxcdn.com/2/svg/1f3a8.svg"/></span><br>
        <a href="https://gitlab.com/michi-zuri">
          <sub>Michael Paul Killian</sub>
        </a>
      </td>
    </tr>
    <tr border="0">
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/raphaelw">
            <img src="https://avatars2.githubusercontent.com/u/7235821?s=460&v=4" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/raphaelw">
          <sub>Raphael</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/alangecker">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/1109912/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/alangecker">
          <sub>Andreas Langecker</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/theolampert">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/2275979/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/theolampert">
          <sub>Theo</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/irgendwer">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/1681670/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Bug reports"><img width="14px" class="emoji" draggable="false" alt="🐜" src="https://twemoji.maxcdn.com/2/svg/1f41c.svg"/></span>&nbsp;<span title="Ideas, Planning, & Feedback"><img width="14px" class="emoji" draggable="false" alt="💡" src="https://twemoji.maxcdn.com/2/svg/1f4a1.svg"/></span>&nbsp;<span title="Security"><img width="14px" class="emoji" draggable="false" alt="🔐" src="https://twemoji.maxcdn.com/2/svg/1f510.svg"/></span><br>
        <a href="https://gitlab.com/irgendwer">
          <sub>irgendwer</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/manuel_w">
            <img src="https://foodsharing.de/images/q_df07990f3897921b5ff18888edf545ac.jpg" width="100px">
          </a><br>
        </div>
        <span title="Board member"><img width="14px" class="emoji" draggable="false" alt="🏢" src="https://twemoji.maxcdn.com/2/svg/1f3e2.svg"/></span>&nbsp;<span title="Ideas, Planning, & Feedback"><img width="14px" class="emoji" draggable="false" alt="💡" src="https://twemoji.maxcdn.com/2/svg/1f4a1.svg"/></span><br>
        <a href="https://gitlab.com/manuel_w">
          <sub>Manuel Wiemann</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/djahnie">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/782504/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Ideas, Planning, & Feedback"><img width="14px" class="emoji" draggable="false" alt="💡" src="https://twemoji.maxcdn.com/2/svg/1f4a1.svg"/></span><br>
        <a href="https://gitlab.com/djahnie">
          <sub>djahnie</sub>
        </a>
      </td>
    </tr>
    <tr border="0">
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/em.ka">
            <img src="https://ca.slack-edge.com/T0B6WCFM5-U0EHB1RP1-07d379bea6ff-512" width="100px">
          </a><br>
        </div>
        <span title="Design"><img width="14px" class="emoji" draggable="false" alt="🎨" src="https://twemoji.maxcdn.com/2/svg/1f3a8.svg"/></span>&nbsp;<span title="Ideas, Planning, & Feedback"><img width="14px" class="emoji" draggable="false" alt="💡" src="https://twemoji.maxcdn.com/2/svg/1f4a1.svg"/></span><br>
        <a href="https://gitlab.com/em.ka">
          <sub>mel</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/EmiliaPaz">
            <img src="https://secure.gravatar.com/avatar/c0370928d12a1dd06716ba813ce4dbcd?s=80&d=identicon" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/EmiliaPaz">
          <sub>Emilia Paz</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/nigeldgreen">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/544783/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/nigeldgreen">
          <sub>Nigel Green</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/D0nPiano">
            <img src="https://ca.slack-edge.com/T0B6WCFM5-U1F4FK22C-c22aeb486bd9-512" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/D0nPiano">
          <sub>D0nPiano</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/adrianheine">
            <img src="https://secure.gravatar.com/avatar/83dd2a385c44fc42d52f14fccd9d992a?s=80&d=identicon" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/adrianheine">
          <sub>Adrian Heine</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/BassTii">
            <img src="https://secure.gravatar.com/avatar/f72182ecbe91d6d60603ec2c31efe7cc?s=80&d=identicon" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/BassTii">
          <sub>Basti A.</sub>
        </a>
      </td>
    </tr>
    <tr border="0">
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/valentin.unicorn">
            <img src="https://secure.gravatar.com/avatar/588c72c402d090166de1bd15a69fdd6b?s=80&d=identicon" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/valentin.unicorn">
          <sub>Valentin</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/inktrap">
            <img src="https://secure.gravatar.com/avatar/ee9f855b89d786169f0413e76ab944e0?s=80&d=identicon" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span>&nbsp;<span title="Documentation"><img width="14px" class="emoji" draggable="false" alt="📝" src="https://twemoji.maxcdn.com/2/svg/1f4dd.svg"/></span><br>
        <a href="https://gitlab.com/inktrap">
          <sub>Valentin</sub>
        </a>
      </td>
      <td border="0" align="center" valign="top" width="16%">
        <div style="height: 100px; width: 100px;">
          <a href="https://gitlab.com/derhuerst">
            <img src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/204799/avatar.png" width="100px">
          </a><br>
        </div>
        <span title="Code"><img width="14px" class="emoji" draggable="false" alt="💻" src="https://twemoji.maxcdn.com/2/svg/1f4bb.svg"/></span><br>
        <a href="https://gitlab.com/derhuerst">
          <sub>Jannis R</sub>
        </a>
      </td>
    </tr>
  </tbody>
</table>
<!-- FOODSHARING-CONTRIBUTORS-LIST:END -->

This project sort of follows the [all-contributors](https://github.com/kentcdodds/all-contributors) specification.
Contributions of any kind are welcome!
