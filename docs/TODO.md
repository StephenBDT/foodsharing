# Todo

 - remove "published with gitbook" link
 - include useful links in the sidebar (everything in one place)

# Done

 - fix script so html files are not in ``docs/``
 - remove fb/twitter icons, include foodsharing icon instead
 - switch serve log level to info

