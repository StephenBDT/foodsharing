<?php

use Foodsharing\DI;

$FS_ENV = getenv('FS_ENV');
$env_filename = 'config.inc.' . $FS_ENV . '.php';
define('FS_ENV', $FS_ENV);

if (file_exists($env_filename)) {
	require_once $env_filename;
} else {
	die('no config found for env [' . $FS_ENV . ']');
}
if (!defined('SOCK_URL')) {
	define('SOCK_URL', 'http://127.0.0.1:1338/');
}

date_default_timezone_set('Europe/Berlin');
locale_set_default('de-DE');
/*
 * Read revision from revision file.
 * It is supposed to define SRC_REVISION.
 */
if (file_exists('revision.inc.php')) {
	require_once 'revision.inc.php';
}

/*
 * Configure Raven (sentry.io client) for remote error reporting
 */
if (defined('SENTRY_URL')) {
	$client = new Raven_Client(SENTRY_URL);
	$client->install();
	$client->tags_context(array('FS_ENV' => $FS_ENV));
	if (defined('SRC_REVISION')) {
		$client->setRelease(SRC_REVISION);
	}
}

if (!defined('RAVEN_JAVASCRIPT_CONFIG') && getenv('RAVEN_JAVASCRIPT_CONFIG')) {
	define('RAVEN_JAVASCRIPT_CONFIG', getenv('RAVEN_JAVASCRIPT_CONFIG'));
}

define('FPDF_FONTPATH', __DIR__ . '/lib/font/');

/* global definitions for Foodsharing\Lib\Func until they might
go away or somewhere else :) */
define('CNT_MAIN', 0);
define('CNT_RIGHT', 1);
define('CNT_TOP', 2);
define('CNT_BOTTOM', 3);
define('CNT_LEFT', 4);
define('CNT_OVERTOP', 5);

define('DSN', 'mysql:host=' . DB_HOST . ';dbname=' . DB_DB . ';charset=utf8');

DI::$shared->compile();

Foodsharing\Lib\Db\Mem::connect();
